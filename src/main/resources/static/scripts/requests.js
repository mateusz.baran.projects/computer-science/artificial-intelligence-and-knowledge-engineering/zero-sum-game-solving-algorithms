var GAME_RUNNING = false;
var WAITING = false;


var xhttp = null;


function move(command) {
    if (GAME_RUNNING && !WAITING) {
        WAITING = true;

        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                WAITING = false;
                engine(this.responseText, true);
            }
        };
        xhttp.open("POST", "http://localhost:8080/api/move", true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(JSON.stringify({"heuristic": getHeuristic(), "command": command}));
    }
}

function aiMove(algorithm) {
    if (GAME_RUNNING && !WAITING) {
        WAITING = true;

        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                WAITING = false;
                engine(this.responseText, true);
            }
        };
        xhttp.open("POST", "http://localhost:8080/api/ai-move", true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send(JSON.stringify({"heuristic": getHeuristic(), "algorithm": algorithm}));
    }
}

function restoreGameState(index) {
    if (xhttp != null) {
        xhttp.abort();
    }

    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            WAITING = false;
            engine(this.responseText, false);
        }

    };
    xhttp.open("POST", "http://localhost:8080/api/restore-game-state", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(JSON.stringify({"heuristic": getHeuristic(), "index": index}));
}