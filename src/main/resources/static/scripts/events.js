var GAME_STARTED = false;

document.getElementById("board-settings-button").addEventListener("click", function (ev) {
    if (GAME_STARTED) {
        window.location.reload();
    } else {
        GAME_STARTED = true;
        GAME_RUNNING = true;
        move("null");
        this.innerText = "Clear board";
    }
});


Array.from(document.getElementsByClassName("circle")).forEach(
    function (element, index, array) {
        element.addEventListener("mouseup", mouseup);
        element.addEventListener("dragstart", dragstart);
        element.addEventListener("dragover", dragover);
        element.addEventListener("drop", drop);
    }
);

function addEventListenerForRecords() {
    Array.from(document.getElementsByClassName("records-move")).forEach(
        function (element, index, array) {
            element.addEventListener("click", function () {
                restoreGameState(this.dataset['id']);

                GAME_STARTED = false;
                GAME_RUNNING = false;
                document.getElementById("board-settings-button").innerText = "Continue";
            });
        }
    );
}

function dragstart(event) {
    event.dataTransfer.setData("text", event.target.id);
    return false;
}

function dragover(event) {
    event.preventDefault();
}

function drop(event) {
    event.preventDefault();
    if (event.target.className.includes("circle")) {
        var command = event.dataTransfer.getData("text") + '-' + event.target.id;
        move(command);
    }
}

function mouseup(event) {
    move(event.target.id);
}
