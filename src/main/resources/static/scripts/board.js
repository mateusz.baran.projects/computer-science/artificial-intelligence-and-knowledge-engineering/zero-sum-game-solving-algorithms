var DELAY = 50;
var CURRENT_PLAYER = "";


function engine(response, loop) {
    if (response) {
        var json = JSON.parse(response);
        CURRENT_PLAYER = json["activePlayerColor"];
        setPointColors(json);
        setMovementRecords(json);
        setInfo(json);
        showEndAlert(json);
        if (loop)
            sendAiMove(json);
    }
}

function sendAiMove() {
    var algorithmWhite = document.getElementById("algorithm-white");
    var algorithmBlack = document.getElementById("algorithm-black");

    var algorithm;
    if (CURRENT_PLAYER === "WHITE") {
        algorithm = algorithmWhite.options[algorithmWhite.selectedIndex].text;
    } else {
        algorithm = algorithmBlack.options[algorithmBlack.selectedIndex].text;
    }

    if (algorithm !== "Human")
        aiMove(algorithm);
}

function showEndAlert(json) {
    if (GAME_RUNNING && json["state"] === "End") {
        GAME_RUNNING = false;
        if (json["winnerPlayerColor"] === "NONE")
            alert("Draw");
        else
            alert("Win " + json["winnerPlayerColor"] + "!");

        console.info(json["result"])
    }
}


function setMovementRecords(json) {
    var whiteRecords = document.getElementById("records-white");
    var blackRecords = document.getElementById("records-black");
    whiteRecords.innerHTML = '';
    blackRecords.innerHTML = '';

    Array.from(json["movementRecords"]).forEach(
        function (element, index, array) {
            if (index > 0) {
                if (index % 2 === 1) {
                    whiteRecords.innerHTML += "<div class='records-move' data-id='" + index + "'>" + index + ": " + element + "</div>";
                } else {
                    blackRecords.innerHTML += "<div class='records-move' data-id='" + index + "'>" + index + ": " + element + "</div>";
                }
            }
        }
    );
    if (!json['restoring']) {
        var recordsBody = document.getElementById("records-body");
        recordsBody.scrollTo(0, recordsBody.scrollHeight);
    }
    addEventListenerForRecords();
}


function setPointColors(json) {
    Object.keys(json["points"]).forEach(
        function (key) {
            var circle = document.getElementById(key);
            var isMoving = json["state"] === "Moving" || json["state"] === "Flying";
            var isRemoving = json["state"] === "Removing";

            if (json["points"][key] === "WHITE") {

                circle.draggable = isMoving && json["activePlayerColor"] === "WHITE";
                if (isRemoving && json["possibleMoves"].includes(key))
                    circle.className = "circle white remove";
                else
                    circle.className = "circle white";

            } else if (json["points"][key] === "BLACK") {

                circle.draggable = isMoving && json["activePlayerColor"] === "BLACK";
                if (isRemoving && json["possibleMoves"].includes(key))
                    circle.className = "circle black remove";
                else
                    circle.className = "circle black";

            } else {
                circle.draggable = false;
                circle.className = "circle none";
            }
        }
    );
}

function setInfo(json) {
    var white = document.getElementById("white-label");
    var black = document.getElementById("black-label");
    if (CURRENT_PLAYER === "WHITE") {
        white.classList.add("player-active");
        black.classList.remove("player-active");
    } else if (CURRENT_PLAYER === "BLACK") {
        white.classList.remove("player-active");
        black.classList.add("player-active");
    } else {
        white.classList.remove("player-active");
        black.classList.remove("player-active");
    }
    if (json['winnerPlayerColor'] === "WHITE") {
        white.classList.add("player-winner");
        black.classList.remove("player-winner");
    } else if (json['winnerPlayerColor'] === "BLACK") {
        white.classList.remove("player-winner");
        black.classList.add("player-winner");
    } else {
        white.classList.remove("player-winner");
        black.classList.remove("player-winner");
    }
    document.getElementById("coefficient").innerText = "Coefficient: " + json["gameStateValue"];
}


function getHeuristic() {
    var heuristic;
    if (CURRENT_PLAYER === "WHITE") {
        var heuristicWhite = document.getElementById("heuristic-white");
        heuristic = heuristicWhite.options[heuristicWhite.selectedIndex].text;
    } else {
        var heuristicBlack = document.getElementById("heuristic-black");
        heuristic = heuristicBlack.options[heuristicBlack.selectedIndex].text;
    }

    return heuristic;
}

