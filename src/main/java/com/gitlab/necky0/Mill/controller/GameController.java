package com.gitlab.necky0.Mill.controller;

import com.gitlab.necky0.Mill.game.Game;
import com.gitlab.necky0.Mill.game.analysis.*;
import com.gitlab.necky0.Mill.game.IGame;
import com.gitlab.necky0.Mill.game.algorithm.Algorithm;
import com.gitlab.necky0.Mill.game.algorithm.AlphaBetaPruning;
import com.gitlab.necky0.Mill.game.algorithm.MiniMax;
import com.gitlab.necky0.Mill.game.model.Classic;
import com.gitlab.necky0.Mill.model.AiMoveBody;
import com.gitlab.necky0.Mill.model.GameState;
import com.gitlab.necky0.Mill.model.MoveBody;
import com.gitlab.necky0.Mill.model.RestoreBody;
import com.gitlab.necky0.Mill.utils.SaverCSV;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class GameController {

    private IGame game;
    private IGame currentGame;
    private int lastSavedIndex;
    private Map<String, GameAnalysis> gameAnalysis;
    private Map<String, Map<String, Algorithm>> algorithms;
    private String timeStamp;
    private SaverCSV saverCSV;

    @RequestMapping(value = "/", method = GET)
    public ModelAndView board() {
        game = new Game(new Classic());
        currentGame = game;

        gameAnalysis = new HashMap<>();
        gameAnalysis.put("AvMovesFlying", new AvMovesFlying());
        gameAnalysis.put("MenDiffAvMoves", new MenDiffAvMoves());
        gameAnalysis.put("MenDiffAvMovesFlying", new MenDiffAvMovesFlying());
        gameAnalysis.put("MenDiffFlying", new MenDiffFlying());

        algorithms = new HashMap<>();

        for (String analysisName : gameAnalysis.keySet()) {
            GameAnalysis analysis = gameAnalysis.get(analysisName);
            Map<String, Algorithm> algorithmMap = new HashMap<>();
            algorithmMap.put("AlphaBeta D1 SFM", new AlphaBetaPruning(game, analysis, 1, true, false));
            algorithmMap.put("AlphaBeta D1 SFM SAM", new AlphaBetaPruning(game, analysis, 1, true, true));
            algorithmMap.put("AlphaBeta D2 SFM", new AlphaBetaPruning(game, analysis, 2, true, false));
            algorithmMap.put("AlphaBeta D2 SFM SAM", new AlphaBetaPruning(game, analysis, 2, true, true));
            algorithmMap.put("AlphaBeta D3 SFM", new AlphaBetaPruning(game, analysis, 3, true, false));
            algorithmMap.put("AlphaBeta D3 SFM SAM", new AlphaBetaPruning(game, analysis, 3, true, true));
            algorithmMap.put("AlphaBeta D4 SFM", new AlphaBetaPruning(game, analysis, 4, true, false));
            algorithmMap.put("AlphaBeta D4 SFM SAM", new AlphaBetaPruning(game, analysis, 4, true, true));
            algorithmMap.put("AlphaBeta D5 SFM", new AlphaBetaPruning(game, analysis, 5, true, false));
            algorithmMap.put("AlphaBeta D5 SFM SAM", new AlphaBetaPruning(game, analysis, 5, true, true));
            algorithmMap.put("AlphaBeta D6 SFM", new AlphaBetaPruning(game, analysis, 6, true, false));
            algorithmMap.put("AlphaBeta D6 SFM SAM", new AlphaBetaPruning(game, analysis, 6, true, true));
            algorithmMap.put("AlphaBeta D7 SFM", new AlphaBetaPruning(game, analysis, 7, true, false));
            algorithmMap.put("AlphaBeta D7 SFM SAM", new AlphaBetaPruning(game, analysis, 7, true, true));
            algorithmMap.put("AlphaBeta D8 SFM", new AlphaBetaPruning(game, analysis, 8, true, false));
            algorithmMap.put("AlphaBeta D8 SFM SAM", new AlphaBetaPruning(game, analysis, 8, true, true));
            algorithmMap.put("MinMax D1 SFM", new MiniMax(game, analysis, 1, true, false));
            algorithmMap.put("MinMax D1 SFM SAM", new MiniMax(game, analysis, 1, true, true));
            algorithmMap.put("MinMax D2 SFM", new MiniMax(game, analysis, 2, true, false));
            algorithmMap.put("MinMax D2 SFM SAM", new MiniMax(game, analysis, 2, true, true));
            algorithmMap.put("MinMax D3 SFM", new MiniMax(game, analysis, 3, true, false));
            algorithmMap.put("MinMax D3 SFM SAM", new MiniMax(game, analysis, 3, true, true));
            algorithmMap.put("MinMax D4 SFM", new MiniMax(game, analysis, 4, true, false));
            algorithmMap.put("MinMax D4 SFM SAM", new MiniMax(game, analysis, 4, true, true));
            algorithmMap.put("MinMax D5 SFM", new MiniMax(game, analysis, 5, true, false));
            algorithmMap.put("MinMax D5 SFM SAM", new MiniMax(game, analysis, 5, true, true));
            algorithmMap.put("AlphaBeta D1", new AlphaBetaPruning(game, analysis, 1, false, false));
            algorithmMap.put("AlphaBeta D2", new AlphaBetaPruning(game, analysis, 2, false, false));
            algorithmMap.put("AlphaBeta D3", new AlphaBetaPruning(game, analysis, 3, false, false));
            algorithmMap.put("AlphaBeta D4", new AlphaBetaPruning(game, analysis, 4, false, false));
            algorithmMap.put("AlphaBeta D5", new AlphaBetaPruning(game, analysis, 5, false, false));
            algorithmMap.put("AlphaBeta D6", new AlphaBetaPruning(game, analysis, 6, false, false));
            algorithmMap.put("AlphaBeta D7", new AlphaBetaPruning(game, analysis, 7, false, false));
            algorithmMap.put("AlphaBeta D8", new AlphaBetaPruning(game, analysis, 8, false, false));
            algorithmMap.put("MinMax D1", new MiniMax(game, analysis, 1, false, false));
            algorithmMap.put("MinMax D2", new MiniMax(game, analysis, 2, false, false));
            algorithmMap.put("MinMax D3", new MiniMax(game, analysis, 3, false, false));
            algorithmMap.put("MinMax D4", new MiniMax(game, analysis, 4, false, false));
            algorithmMap.put("MinMax D5", new MiniMax(game, analysis, 5, false, false));
            algorithms.put(analysisName, algorithmMap);
        }

        timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());

        saverCSV = new SaverCSV();
        saverCSV.add("Timestamp");
        saverCSV.add("Move number");
        saverCSV.add("Move");
        saverCSV.add("Color");
        saverCSV.add("Game State");
        saverCSV.add("Game Result");
        saverCSV.add("Heuristic");
        saverCSV.add("Current Value");
        saverCSV.add("Algorithm");
        saverCSV.add("Iterations");
        saverCSV.add("Time");
        saverCSV.addLastInRow("Predict Value");
        lastSavedIndex = 0;

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("algorithms", new TreeSet<>(algorithms.get("AvMovesFlying").keySet()));
        modelAndView.addObject("heuristics", new TreeSet<>(gameAnalysis.keySet()));
        modelAndView.setViewName("board");
        return modelAndView;
    }

    @RequestMapping(value = "/api/move", method = POST)
    public GameState move(@RequestBody() MoveBody moveBody) {
        if (game != null) {
            game.move(moveBody.getCommand());
            saveToCSV(moveBody.getHeuristic(), null);

            return gameState(false, moveBody.getHeuristic());
        }

        return null;
    }

    @RequestMapping(value = "/api/ai-move", method = POST)
    public GameState aiMove(@RequestBody() AiMoveBody aiMoveBody) {
        if (game != null) {
            IGame gameTemp = game;
            Algorithm algorithm = algorithms.get(aiMoveBody.getHeuristic()).get(aiMoveBody.getAlgorithm());
            algorithm.setGame(gameTemp);
            String command = algorithm.getNextMove();
            if (command != null) {
                gameTemp.move(command);
                saveToCSV(aiMoveBody.getHeuristic(), aiMoveBody.getAlgorithm());
            }

            return gameState(false, aiMoveBody.getHeuristic());
        }

        return null;
    }

    @RequestMapping(value = "/api/restore-game-state", method = POST)
    public GameState restoreGameState(@RequestBody() RestoreBody restoreBody) {
        if (currentGame != null && restoreBody.getIndex() >= 0 && restoreBody.getIndex() <= currentGame.getGameStateLastIndex()) {
            game = currentGame.getGameState(restoreBody.getIndex());
            if (game != null && restoreBody.getIndex() == currentGame.getGameStateLastIndex())
                game = currentGame;
            else
                game = currentGame.getGameState(restoreBody.getIndex());
            return gameState(true, restoreBody.getHeuristic());
        }

        return null;
    }

    @RequestMapping(value = "/api/download", method = RequestMethod.GET)
    public String getFile(HttpServletResponse response) {
        try {
            String filename = String.format("game_%s.csv", timeStamp);

            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", filename));
            response.flushBuffer();
        } catch (IOException ignored) {}
        return saverCSV.toString();
    }

    private GameState gameState(boolean restoring, String heuristic) {
        Map<String, String> points = new HashMap<>();
        for (String point : game.getModel().getPointColor().keySet())
            points.put(point, game.getModel().getPointColor().get(point).name());

        double value = gameAnalysis.containsKey(heuristic) ? gameAnalysis.get(heuristic).coefficient(game) : 0;

        return new GameState(points, game.getActivePlayerColor().name(),
                game.getWinnerPlayerColor().name(), game.getPossibleMoves(),
                currentGame.getMovementRecords(), game.getPlacingMenLeft(), game.getState().name(),
                game.getResult().name(), value, restoring);
    }

    private void saveToCSV(String heuristic, String algorithm) {
        if (lastSavedIndex < currentGame.getGameStateLastIndex()) {
            lastSavedIndex = currentGame.getGameStateLastIndex();
            Game prev = currentGame.getGameState(lastSavedIndex - 1);

            saverCSV.add(timeStamp);
            saverCSV.add(String.valueOf(lastSavedIndex));
            saverCSV.add(currentGame.getMovementRecord());
            saverCSV.add(prev.getActivePlayerColor().name());
            saverCSV.add(prev.getState().name());
            saverCSV.add(currentGame.getResult().name());

            if (heuristic != null) {
                saverCSV.add(heuristic);
                saverCSV.add(String.valueOf(gameAnalysis.get(heuristic).coefficient(currentGame)));
            } else {
                saverCSV.add("null");
                saverCSV.add("null");
            }

            if (heuristic != null && algorithm != null) {
                Algorithm alg = algorithms.get(heuristic).get(algorithm);
                saverCSV.add(algorithm);
                saverCSV.add(String.valueOf(alg.getIterations()));
                saverCSV.add(String.valueOf(alg.getTime()));
                saverCSV.addLastInRow(String.valueOf(alg.getValue()));
            } else {
                saverCSV.add("null");
                saverCSV.add("null");
                saverCSV.add("null");
                saverCSV.addLastInRow("null");
            }
        }
    }

}
