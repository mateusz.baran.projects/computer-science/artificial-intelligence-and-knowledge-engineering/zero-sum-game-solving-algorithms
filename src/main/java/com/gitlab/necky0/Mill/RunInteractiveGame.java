package com.gitlab.necky0.Mill;

import com.gitlab.necky0.Mill.game.Game;
import com.gitlab.necky0.Mill.game.analysis.GameAnalysis;
import com.gitlab.necky0.Mill.game.analysis.MenDiffAvMovesFlying;
import com.gitlab.necky0.Mill.game.IGame;
import com.gitlab.necky0.Mill.game.algorithm.MiniMax;
import com.gitlab.necky0.Mill.game.model.Classic;
import com.gitlab.necky0.Mill.game.model.Model;
import com.gitlab.necky0.Mill.game.model.enumerator.State;

import java.util.Scanner;

public class RunInteractiveGame {
    public static void main(String[] args) {

        Model model = new Classic();
        IGame game = new Game(model);

        GameAnalysis gameStateAnalysis = new MenDiffAvMovesFlying();

        MiniMax miniMax = new MiniMax(game, gameStateAnalysis, 1, true, false);

        Scanner scanner = new Scanner(System.in);
        String command;
        do {
            System.out.println();
            System.out.println(game.getModel());
            System.out.println("Phase: " + game.getState());
            System.out.println("Player: " + game.getActivePlayerColor());
            System.out.println("Available moves: " + game.getPossibleMoves());
            System.out.println("Coefficient: " + gameStateAnalysis.coefficient(game));
            System.out.println("Active Player Coefficient: " + gameStateAnalysis.activePlayerCoefficient(game));
            System.out.println("Best Move: " + miniMax.getNextMove());

            if (State.End.equals(game.getState())) {
                System.out.println();
                System.out.println("--- WINNER ---");
                System.out.println("--> " + game.getResult());
                System.out.println("--> " + game.getWinnerPlayerColor());
                break;
            }

            System.out.print("\nEnter your move: ");
            command = scanner.nextLine();
            if ("back".equals(command) && game.getGameStateLastIndex() > 0) {
                game = game.getGameState(game.getGameStateLastIndex() - 1);
                miniMax.setGame(game);
            } else {
                game.move(command);
            }

        } while (!"exit".equals(command));
    }
}
