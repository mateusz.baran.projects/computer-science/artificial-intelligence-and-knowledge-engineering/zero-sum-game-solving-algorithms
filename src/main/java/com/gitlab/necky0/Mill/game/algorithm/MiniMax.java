package com.gitlab.necky0.Mill.game.algorithm;

import com.gitlab.necky0.Mill.game.analysis.GameAnalysis;
import com.gitlab.necky0.Mill.game.analysis.MenDiffAvMovesFlying;
import com.gitlab.necky0.Mill.game.IGame;
import com.gitlab.necky0.Mill.game.model.enumerator.Color;
import com.gitlab.necky0.Mill.game.model.enumerator.State;

import java.util.List;

public class MiniMax extends Algorithm {

    public MiniMax(IGame game, GameAnalysis gameAnalysis, int maxDepth, boolean sortFirstMove, boolean sortAllMoves) {
        super(game, gameAnalysis, maxDepth, sortFirstMove, sortAllMoves);
    }

    @Override
    public String getNextMove() {
        iterations = 0;
        long start = System.nanoTime();

        String bestMove = null;
        double result;
        value = -Double.MAX_VALUE;

        Color playerColor = game.getActivePlayerColor();
        List<String> possibleMoves = getPossibleMoves();
        if (sortFirstMove)
            sortPossibleMoves(possibleMoves, playerColor, true);

        for (String nextMove : possibleMoves) {
            result = algorithm(game.copy(), nextMove, 1, playerColor);

            if (bestMove == null || result > value) {
                bestMove = nextMove;
                value = result;
            }
        }

        long stop = System.nanoTime();
        time = (double) (stop - start) / 1_000_000_000.0;

        return bestMove;
    }

    private double algorithm(IGame game, String move, int depth, Color playerColor) {
        game.move(move);
        iterations += 1;

        if (game.getState() == State.End)
            if (game.getWinnerPlayerColor() == playerColor)
                return Double.MAX_VALUE / depth;
            else if (game.getWinnerPlayerColor() == Color.NONE)
                return MenDiffAvMovesFlying.DRAW_REWARD;
            else
                return -Double.MAX_VALUE / depth;
        else if (!game.getState().equals(State.Removing) && depth >= maxDepth)
            return gameAnalysis.playerCoefficient(game, playerColor);
        else {
            double result;
            boolean maximizing = game.getActivePlayerColor() == playerColor;
            double bestResult = maximizing ? -Double.MAX_VALUE : Double.MAX_VALUE;

            List<String> possibleMoves = game.getPossibleMoves();
            if (sortAllMoves)
                sortPossibleMoves(possibleMoves, playerColor, maximizing);

            for (String nextMove : possibleMoves) {
                result = algorithm(game.copy(), nextMove, depth + 1, playerColor);

                if (maximizing) {
                    if (result > bestResult)
                        bestResult = result;
                } else {
                    if (result < bestResult)
                        bestResult = result;
                }
            }
            return bestResult;
        }
    }

}
