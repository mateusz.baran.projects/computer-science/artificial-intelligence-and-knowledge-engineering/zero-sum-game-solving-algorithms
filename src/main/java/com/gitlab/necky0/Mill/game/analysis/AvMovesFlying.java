package com.gitlab.necky0.Mill.game.analysis;


import com.gitlab.necky0.Mill.game.IGame;

public class AvMovesFlying extends GameAnalysis {

    public AvMovesFlying() {
    }

    public double coefficient(IGame game) {
        return (availableMovesFactor(game)) / menInGameFactor(game);
    }

}
