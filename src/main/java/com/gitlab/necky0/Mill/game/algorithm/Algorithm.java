package com.gitlab.necky0.Mill.game.algorithm;


import com.gitlab.necky0.Mill.game.IGame;
import com.gitlab.necky0.Mill.game.analysis.GameAnalysis;
import com.gitlab.necky0.Mill.game.model.enumerator.Color;

import java.util.ArrayList;
import java.util.List;

public abstract class Algorithm {
    protected IGame game;
    protected GameAnalysis gameAnalysis;
    protected int maxDepth;
    protected boolean sortFirstMove;
    protected boolean sortAllMoves;
    protected double value;
    protected int iterations;
    protected double time;

    Algorithm(IGame game, GameAnalysis gameAnalysis, int maxDepth, boolean sortFirstMove, boolean sortAllMoves) {
        this.game = game;
        this.gameAnalysis = gameAnalysis;
        this.maxDepth = maxDepth;
        this.sortFirstMove = sortFirstMove;
        this.sortAllMoves = sortAllMoves;
    }

    public double getValue() {
        return value;
    }

    public int getIterations() {
        return iterations;
    }

    public double getTime() {
        return time;
    }

    public void setGame(IGame game) {
        this.game = game;
    }

    public abstract String getNextMove();

    List<String> getPossibleMoves() {
        List<String> possibleMoves = new ArrayList<>();
        for (String nextMove : game.getPossibleMoves())
            possibleMoves.addAll(getPossibleMoves(game.copy(), nextMove));

        return possibleMoves;
    }

    private List<String> getPossibleMoves(IGame game, String move) {
        List<String> possibleMoves = new ArrayList<>();
        game.move(move);
        if (game.getRemovingMenLeft() == 0)
            possibleMoves.add(game.getMovementRecord());
        else
            for (String m : game.getPossibleMoves())
                possibleMoves.addAll(getPossibleMoves(game.copy(), m));

        return possibleMoves;
    }

    void sortPossibleMoves(List<String> possibleMoves, Color playerColor, boolean descending) {
        List<Double> values = new ArrayList<>();
        for (String nextMove : possibleMoves) {
            IGame igame = game.copy();
            igame.move(nextMove);
            values.add(gameAnalysis.playerCoefficient(igame, playerColor));
        }

        bubbleSort(values, possibleMoves, descending);
    }

    private void bubbleSort(List<Double> values, List<String> moves, boolean descending) {
        for (int i = 0; i < values.size() - 1; i++) {
            for (int j = i + 1; j < values.size(); j++) {
                if (descending) {
                    if (values.get(i) < values.get(j)) {
                        replace(values, i, j);
                        replace(moves, i, j);
                    }
                } else {
                    if (values.get(i) > values.get(j)) {
                        replace(values, i, j);
                        replace(moves, i, j);
                    }
                }
            }
        }
    }

    private <T> void replace(List<T> array, int i1, int i2) {
        T temp = array.get(i1);
        array.set(i1, array.get(i2));
        array.set(i2, temp);
    }
}
