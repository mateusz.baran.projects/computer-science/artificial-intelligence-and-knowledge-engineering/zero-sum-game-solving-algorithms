package com.gitlab.necky0.Mill.game.analysis;


import com.gitlab.necky0.Mill.game.IGame;

public class MenDiffAvMovesFlying extends GameAnalysis {

    public MenDiffAvMovesFlying() {
    }

    public double coefficient(IGame game) {
        return (menDifferenceFactor(game) + availableMovesFactor(game)) / menInGameFactor(game);
    }

}
