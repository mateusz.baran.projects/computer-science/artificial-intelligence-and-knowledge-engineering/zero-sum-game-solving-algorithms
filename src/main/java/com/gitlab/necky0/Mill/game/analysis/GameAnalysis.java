package com.gitlab.necky0.Mill.game.analysis;

import com.gitlab.necky0.Mill.game.IGame;
import com.gitlab.necky0.Mill.game.model.enumerator.Color;

public abstract class GameAnalysis {

    public static final double DRAW_REWARD = -2;

    public static final double MIN_MEN_FACTOR = 4.1;
    public static final double MAN_FACTOR = 5;
    public static final double FREE_NEIGHBOUR_FACTOR = 2;
    public static final double ALLY_NEIGHBOUR_FACTOR = -.5;

    public GameAnalysis() {
    }

    private double playerTurn(IGame game) {
        return game.getActivePlayerColor() == Color.WHITE ? 1 : -1;
    }

    private double playerTurn(Color playerColor) {
        return playerColor == Color.WHITE ? 1 : -1;
    }

    public abstract double coefficient(IGame game);

    public double activePlayerCoefficient(IGame game) {
        return playerTurn(game) * coefficient(game);
    }

    public double playerCoefficient(IGame game, Color playerColor) {
        return playerTurn(playerColor) * coefficient(game);
    }

    protected double availableMovesFactor(IGame game) {
        return countAvailableMoves(game, Color.WHITE) - countAvailableMoves(game, Color.BLACK);
    }

    protected double menDifferenceFactor(IGame game) {
//        if (game.getPlacingMenLeft() > 0)
//            return MAN_FACTOR * (game.getModel().getPointsByColor().get(Color.WHITE).size() - game.getModel().getPointsByColor().get(Color.BLACK).size());
        return MAN_FACTOR * (Math.max(game.getModel().getPointsByColor().get(Color.WHITE).size(), MIN_MEN_FACTOR) - Math.max(game.getModel().getPointsByColor().get(Color.BLACK).size(), MIN_MEN_FACTOR));
    }

    protected double menInGameFactor(IGame game) {
        return game.getPlacingMenLeft() + MAN_FACTOR;
    }

    private double countAvailableMoves(IGame game, Color playerColor) {
        double sum = 0;
        for (String pointName : game.getModel().getPointsByColor().get(playerColor)) {
            for (String neighbourName : game.getModel().getPointNeighbours().get(pointName))
                if (game.getModel().getPointColor().get(neighbourName) == Color.NONE)
                    sum += FREE_NEIGHBOUR_FACTOR;
                else if (game.getModel().getPointColor().get(neighbourName) == playerColor)
                    sum += ALLY_NEIGHBOUR_FACTOR;
        }
        return sum;
    }
}
