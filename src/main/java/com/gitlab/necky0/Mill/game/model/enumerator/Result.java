package com.gitlab.necky0.Mill.game.model.enumerator;

public enum Result {
    None, NoPossibleMoves, NotEnoughMen, ExceededMovesWithoutMill, RepeatedPosition
}
