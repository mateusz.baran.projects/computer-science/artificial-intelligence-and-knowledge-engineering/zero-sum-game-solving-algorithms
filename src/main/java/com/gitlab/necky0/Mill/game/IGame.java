package com.gitlab.necky0.Mill.game;

import com.gitlab.necky0.Mill.game.model.Model;
import com.gitlab.necky0.Mill.game.model.enumerator.Color;
import com.gitlab.necky0.Mill.game.model.enumerator.Result;
import com.gitlab.necky0.Mill.game.model.enumerator.State;

import java.util.List;

public interface IGame {

    void move(String command);

    int getGameStateLastIndex();

    Game getGameState(int index);

    List<String> getPossibleMoves();

    Model getModel();

    String getMovementRecord();

    List<String> getMovementRecords();

    int getPlacingMenLeft();

    int getRemovingMenLeft();

    int getMovesWithoutMill();

    String[] getPreviousMove();

    Color getActivePlayerColor();

    Color getInactivePlayerColor();

    Color getWinnerPlayerColor();

    State getState();

    Result getResult();

    IGame copy();
}
