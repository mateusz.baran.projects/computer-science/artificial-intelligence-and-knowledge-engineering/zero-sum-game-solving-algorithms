package com.gitlab.necky0.Mill.game.algorithm;

import com.gitlab.necky0.Mill.game.analysis.GameAnalysis;
import com.gitlab.necky0.Mill.game.analysis.MenDiffAvMovesFlying;
import com.gitlab.necky0.Mill.game.IGame;
import com.gitlab.necky0.Mill.game.model.enumerator.Color;
import com.gitlab.necky0.Mill.game.model.enumerator.State;

import java.util.List;

public class AlphaBetaPruning extends Algorithm {

    public AlphaBetaPruning(IGame game, GameAnalysis gameAnalysis, int maxDepth, boolean sortFirstMove, boolean sortAllMoves) {
        super(game, gameAnalysis, maxDepth, sortFirstMove, sortAllMoves);
    }

    @Override
    public String getNextMove() {
        iterations = 0;
        long start = System.nanoTime();

        String bestMove = null;
        value = -Double.MAX_VALUE;

        double alpha = -Double.MAX_VALUE;
        double beta = Double.MAX_VALUE;

        Color playerColor = game.getActivePlayerColor();
        List<String> possibleMoves = getPossibleMoves();
        if (sortFirstMove)
            sortPossibleMoves(possibleMoves, playerColor, true);

        for (String nextMove : possibleMoves) {
            alpha = algorithm(game.copy(), nextMove, 1, playerColor, alpha, beta);

            if (bestMove == null || alpha > value) {
                bestMove = nextMove;
                value = alpha;
            }
        }

        long stop = System.nanoTime();
        time = (double) (stop - start) / 1_000_000_000.0;

        return bestMove;
    }

    private double algorithm(IGame game, String move, int depth, Color playerColor, double alpha, double beta) {
        game.move(move);
        iterations += 1;

        if (game.getState() == State.End)
            if (game.getWinnerPlayerColor() == playerColor)
                return Double.MAX_VALUE / depth;
            else if (game.getWinnerPlayerColor() == Color.NONE)
                return MenDiffAvMovesFlying.DRAW_REWARD;
            else
                return -Double.MAX_VALUE / depth;
        else if (!game.getState().equals(State.Removing) && depth >= maxDepth)
            return gameAnalysis.playerCoefficient(game, playerColor);
        else {
            double result;
            boolean maximizing = game.getActivePlayerColor() == playerColor;
            double bestResult = maximizing ? alpha : beta;

            List<String> possibleMoves = game.getPossibleMoves();
            if (sortAllMoves)
                sortPossibleMoves(possibleMoves, playerColor, maximizing);

            for (String nextMove : possibleMoves) {
                result = algorithm(game.copy(), nextMove, depth + 1, playerColor, alpha, beta);

                if (maximizing) {
                    if (result > bestResult) {
                        alpha = result;
                        bestResult = result;
                    }
                } else {
                    if (result < bestResult) {
                        beta = result;
                        bestResult = result;
                    }
                }

                if (alpha >= beta)
                    break;
            }

            return bestResult;
        }
    }
}
