package com.gitlab.necky0.Mill.game.analysis;


import com.gitlab.necky0.Mill.game.IGame;

public class MenDiffFlying extends GameAnalysis {

    public MenDiffFlying() {
    }

    public double coefficient(IGame game) {
        return (menDifferenceFactor(game)) / menInGameFactor(game);
    }

}
