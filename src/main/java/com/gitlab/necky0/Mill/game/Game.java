package com.gitlab.necky0.Mill.game;

import com.gitlab.necky0.Mill.game.model.Model;
import com.gitlab.necky0.Mill.game.model.enumerator.*;

import java.util.*;

public class Game implements IGame {
    private final static String MOVE_SEPARATOR = "-";
    private final static String REMOVE_SEPARATOR = "/";
    private final static int MOVES_WITHOUT_MILL = 50;

    private Model model;
    private List<Boolean> millsFlags;

    private String movementRecord;
    private List<String> movementRecords;
    private List<Game> gameHistory;

    private int placingMenLeft;
    private int removingMenLeft;
    private int movesWithoutMill;

    private List<String> possibleMoves;
    private Map<Color, String[]> previousMove;

    private Color activePlayerColor;
    private Color inactivePlayerColor;
    private Color winnerPlayerColor;

    private State state;
    private Result result;


    public Game(Model model) {
        this.model = model.copy();

        this.millsFlags = new ArrayList<>();
        for (int i = 0; i < model.getMills().size(); i++)
            millsFlags.add(false);

        this.movementRecord = "";
        this.movementRecords = new ArrayList<>();
        this.gameHistory = new ArrayList<>();

        this.placingMenLeft = model.getNumberOfMen();
        this.removingMenLeft = 0;
        this.movesWithoutMill = 0;

        this.possibleMoves = null;
        this.previousMove = new HashMap<>();
        this.previousMove.put(Color.WHITE, new String[]{"", ""});
        this.previousMove.put(Color.BLACK, new String[]{"", ""});

        this.activePlayerColor = Color.WHITE;
        this.inactivePlayerColor = Color.BLACK;
        this.winnerPlayerColor = Color.NONE;

        this.result = Result.None;
        this.state = State.Placing;

        saveGameState();
    }


    private Game(Game game) {
        this.model = game.model.copy();
        this.millsFlags = new ArrayList<>(game.millsFlags);
        this.movementRecord = game.movementRecord;
        this.movementRecords = new ArrayList<>(game.movementRecords);
        this.placingMenLeft = game.placingMenLeft;
        this.removingMenLeft = game.removingMenLeft;
        this.movesWithoutMill = game.movesWithoutMill;
        this.previousMove = new HashMap<>(game.previousMove);
        this.activePlayerColor = game.activePlayerColor;
        this.inactivePlayerColor = game.inactivePlayerColor;
        this.winnerPlayerColor = game.winnerPlayerColor;
        this.state = game.state;
        this.result = game.result;
        this.gameHistory = new ArrayList<>(game.gameHistory);
        this.possibleMoves = game.possibleMoves == null ? null : new ArrayList<>(game.possibleMoves);
    }

    @Override
    public void move(String commands) {
        for (String command : commands.split(REMOVE_SEPARATOR)) {
            if (getPossibleMoves().contains(command)) {
                switch (state) {
                    case Placing:
                        placing(command);
                        break;
                    case Moving:
                    case Flying:
                        moving(command);
                        break;
                    case Removing:
                        removing(command);
                        break;
                }

                possibleMoves = null;
                setState();
                checkEnd();
                saveGameState();
            } else {
                break;
            }
        }
    }

    @Override
    public int getGameStateLastIndex() {
        return gameHistory.size() - 1;
    }

    @Override
    public Game getGameState(int index) {
        return gameHistory.get(index).copy();
    }

    @Override
    public List<String> getPossibleMoves() {
        if (possibleMoves != null)
            return possibleMoves;

        switch (state) {
            case Placing:
                possibleMoves = getPossiblePlacingMoves();
                break;
            case Moving:
                possibleMoves = getPossibleMovingMoves();
                break;
            case Flying:
                possibleMoves = getPossibleFlyingMoves();
                break;
            case Removing:
                possibleMoves = getPossibleRemovingMoves();
                break;
            default:
                possibleMoves = new ArrayList<>();
                break;
        }
        return possibleMoves;
    }

    private void saveGameState() {
        if (removingMenLeft == 0) {
            gameHistory.add(this.copy());
            movementRecords.add(movementRecord);
        }
    }

    private void moving(String command) {
        String[] points = command.split(MOVE_SEPARATOR);
        model.setPointColor(points[1], model.getPointColor().get(points[0]));
        model.setPointColor(points[0], Color.NONE);
        movesWithoutMill += 1;
        removingMenLeft = countMills();
        movementRecord = command;
        if (!model.isBackwardMoves())
            previousMove.put(activePlayerColor, points);
    }

    private void placing(String point) {
        model.setPointColor(point, activePlayerColor);
        placingMenLeft -= 1;
        removingMenLeft = countMills();
        movementRecord = point;
    }

    private void removing(String point) {
        model.setPointColor(point, Color.NONE);
        removingMenLeft -= 1;
        movementRecord += REMOVE_SEPARATOR + point;
    }

    private void checkEnd() {
        if (model.getPointsByColor().get(Color.WHITE).size() + placingMenLeft < 3) {
            winnerPlayerColor = Color.BLACK;
            result = Result.NotEnoughMen;
            state = State.End;
        } else if (model.getPointsByColor().get(Color.BLACK).size() + placingMenLeft < 3) {
            winnerPlayerColor = Color.WHITE;
            result = Result.NotEnoughMen;
            state = State.End;
        } else if (getPossibleMoves().size() == 0) {
            winnerPlayerColor = inactivePlayerColor;
            result = Result.NoPossibleMoves;
            state = State.End;
        } else if (movesWithoutMill >= MOVES_WITHOUT_MILL) {
            result = Result.ExceededMovesWithoutMill;
            state = State.End;
        } else if (checkRepeatPositions()) {
            result = Result.RepeatedPosition;
            state = State.End;
        }
    }

    private boolean checkRepeatPositions() {
        int sum = 0;
        for (Game game : gameHistory)
            if (game.model.getPointColor().equals(model.getPointColor()))
                sum += 1;
        return sum > 1;
    }

    private int countMills() {
        int nbMills = 0;
        for (int i = 0; i < model.getMills().size(); i++) {
            boolean flag = checkMill(model.getMills().get(i));
            if (flag && !millsFlags.get(i)) {
                movesWithoutMill = 0;
                nbMills += 1;
            }
            millsFlags.set(i, flag);
        }
        return nbMills;
    }

    private boolean checkMill(Set<String> points) {
        Color color, prevColor = null;
        for (String pointName : points) {
            color = model.getPointColor().get(pointName);
            if (color == Color.NONE || prevColor != null && color != prevColor)
                return false;
            prevColor = color;
        }
        return true;
    }

    private void setState() {
        if (state != State.End) {
            if (removingMenLeft > 0) {
                state = State.Removing;
            } else {
                swapPlayers();

                if (placingMenLeft > 0) {
                    state = State.Placing;
                } else if (model.getPointsByColor().get(activePlayerColor).size() == 3) {
                    state = State.Flying;
                } else {
                    state = State.Moving;
                }
            }
        }
    }

    private void swapPlayers() {
        Color playerColor = activePlayerColor;
        activePlayerColor = inactivePlayerColor;
        inactivePlayerColor = playerColor;
    }

    private List<String> getPossiblePlacingMoves() {
        return new ArrayList<>(model.getPointsByColor().get(Color.NONE));
    }

    private List<String> getPossibleMovingMoves() {
        List<String> moves = new ArrayList<>();
        for (String pointName : model.getPointsByColor().get(activePlayerColor))
            for (String neighbourName : model.getPointNeighbours().get(pointName)) {
                if (model.getPointColor().get(neighbourName) == Color.NONE
                        && (!pointName.equals(previousMove.get(activePlayerColor)[1])
                        || !neighbourName.equals(previousMove.get(activePlayerColor)[0])))
                    moves.add(pointName + MOVE_SEPARATOR + neighbourName);
            }
        return moves;
    }

    private List<String> getPossibleFlyingMoves() {
        List<String> moves = new ArrayList<>();
        for (String pointName : model.getPointsByColor().get(activePlayerColor))
            for (String neighbourName : model.getPointsByColor().get(Color.NONE))
                if (!pointName.equals(previousMove.get(activePlayerColor)[1])
                        || !neighbourName.equals(previousMove.get(activePlayerColor)[0]))
                    moves.add(pointName + MOVE_SEPARATOR + neighbourName);
        return moves;
    }

    private List<String> getPossibleRemovingMoves() {
        List<String> moves = new ArrayList<>();
        for (String pointName : model.getPointsByColor().get(inactivePlayerColor))
            if (!isMillContains(pointName))
                moves.add(pointName);

        if (moves.size() == 0)
            moves.addAll(model.getPointsByColor().get(inactivePlayerColor));

        return moves;
    }

    private boolean isMillContains(String pointName) {
        for (int i = 0; i < millsFlags.size(); i++)
            if (millsFlags.get(i) && model.getMills().get(i).contains(pointName))
                return true;
        return false;
    }

    @Override
    public Model getModel() {
        return model;
    }

    @Override
    public String getMovementRecord() {
        return movementRecord;
    }

    @Override
    public List<String> getMovementRecords() {
        return new ArrayList<>(movementRecords);
    }

    @Override
    public int getPlacingMenLeft() {
        return placingMenLeft;
    }

    @Override
    public int getRemovingMenLeft() {
        return removingMenLeft;
    }

    @Override
    public int getMovesWithoutMill() {
        return movesWithoutMill;
    }

    @Override
    public String[] getPreviousMove() {
        return previousMove.get(inactivePlayerColor).clone();
    }

    @Override
    public Color getActivePlayerColor() {
        return activePlayerColor;
    }

    @Override
    public Color getInactivePlayerColor() {
        return inactivePlayerColor;
    }

    @Override
    public Color getWinnerPlayerColor() {
        return winnerPlayerColor;
    }

    @Override
    public State getState() {
        return state;
    }

    @Override
    public Result getResult() {
        return result;
    }

    @Override
    public Game copy() {
        return new Game(this);
    }
}
