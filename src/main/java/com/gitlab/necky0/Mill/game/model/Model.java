package com.gitlab.necky0.Mill.game.model;

import com.gitlab.necky0.Mill.game.model.enumerator.Color;

import java.io.Serializable;
import java.util.*;

public abstract class Model implements Serializable {
    protected Map<String, Color> pointColor;
    protected Map<Color, Set<String>> pointsByColor;
    protected Map<String, Set<String>> pointNeighbours;
    protected List<Set<String>> mills;
    protected int numberOfMen;
    protected boolean backwardMoves;

    public Model(int numberOfMen, boolean backwardMoves) {
        this.pointColor = new HashMap<>();
        this.pointsByColor = new HashMap<>();
        this.pointNeighbours = new HashMap<>();
        this.mills = new ArrayList<>();
        this.numberOfMen = numberOfMen;
        this.backwardMoves = backwardMoves;

        initPoints();
        initNeighbours();
        initMills();
        initPointsByColor();
    }

    public Model(Model model) {
        this.pointColor = new HashMap<>(model.getPointColor());
        this.pointsByColor = new HashMap<>();
        for (Color color : model.getPointsByColor().keySet())
            this.pointsByColor.put(color, new HashSet<>(model.getPointsByColor().get(color)));
        this.pointNeighbours = model.getPointNeighbours();
        this.mills = model.getMills();
        this.numberOfMen = model.getNumberOfMen();
        this.backwardMoves = model.isBackwardMoves();
    }

    public abstract Model copy();

    protected abstract void initPoints();

    protected abstract void initNeighbours();

    protected abstract void initMills();

    public Map<String, Color> getPointColor() {
        return pointColor;
    }

    public Map<Color, Set<String>> getPointsByColor() {
        return pointsByColor;
    }

    public Map<String, Set<String>> getPointNeighbours() {
        return pointNeighbours;
    }

    public List<Set<String>> getMills() {
        return mills;
    }

    public int getNumberOfMen() {
        return numberOfMen;
    }

    public boolean isBackwardMoves() {
        return backwardMoves;
    }

    private void initPointsByColor() {
        for (Color color : Color.values()) {
            Set<String> pts = new HashSet<>();
            for (String name : pointColor.keySet())
                if (pointColor.get(name) == color)
                    pts.add(name);
            pointsByColor.put(color, pts);
        }
    }

    public void setPointColor(String pointName, Color color) {
        pointsByColor.get(pointColor.get(pointName)).remove(pointName);
        pointsByColor.get(color).add(pointName);
        pointColor.put(pointName, color);
    }
}
