package com.gitlab.necky0.Mill.game.model.enumerator;

public enum State {
    Placing, Moving, Flying, Removing, End
}
