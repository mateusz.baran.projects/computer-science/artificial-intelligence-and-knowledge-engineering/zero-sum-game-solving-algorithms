package com.gitlab.necky0.Mill.game.model;

import com.gitlab.necky0.Mill.game.model.enumerator.Color;

import java.util.*;

public class Classic extends Model {

    public Classic() {
        super(18, true);
    }

    private Classic(Model model) {
        super(model);
    }

    @Override
    public Model copy() {
        return new Classic(this);
    }

    @Override
    protected void initPoints() {
        pointColor.put("a1", Color.NONE);
        pointColor.put("a4", Color.NONE);
        pointColor.put("a7", Color.NONE);

        pointColor.put("b2", Color.NONE);
        pointColor.put("b4", Color.NONE);
        pointColor.put("b6", Color.NONE);

        pointColor.put("c3", Color.NONE);
        pointColor.put("c4", Color.NONE);
        pointColor.put("c5", Color.NONE);

        pointColor.put("d1", Color.NONE);
        pointColor.put("d2", Color.NONE);
        pointColor.put("d3", Color.NONE);
        pointColor.put("d5", Color.NONE);
        pointColor.put("d6", Color.NONE);
        pointColor.put("d7", Color.NONE);

        pointColor.put("e3", Color.NONE);
        pointColor.put("e4", Color.NONE);
        pointColor.put("e5", Color.NONE);

        pointColor.put("f2", Color.NONE);
        pointColor.put("f4", Color.NONE);
        pointColor.put("f6", Color.NONE);

        pointColor.put("g1", Color.NONE);
        pointColor.put("g4", Color.NONE);
        pointColor.put("g7", Color.NONE);
    }

    @Override
    protected void initNeighbours() {
        pointNeighbours.put("a1", new HashSet<>(Arrays.asList("a4", "d1")));
        pointNeighbours.put("a4", new HashSet<>(Arrays.asList("a1", "a7", "b4")));
        pointNeighbours.put("a7", new HashSet<>(Arrays.asList("a4", "d7")));

        pointNeighbours.put("b2", new HashSet<>(Arrays.asList("d2", "b4")));
        pointNeighbours.put("b4", new HashSet<>(Arrays.asList("b2", "a4", "c4", "b6")));
        pointNeighbours.put("b6", new HashSet<>(Arrays.asList("b4", "d6")));

        pointNeighbours.put("c3", new HashSet<>(Arrays.asList("c4", "d3")));
        pointNeighbours.put("c4", new HashSet<>(Arrays.asList("c3", "c5", "b4")));
        pointNeighbours.put("c5", new HashSet<>(Arrays.asList("c4", "d5")));

        pointNeighbours.put("d1", new HashSet<>(Arrays.asList("a1", "g1", "d2")));
        pointNeighbours.put("d2", new HashSet<>(Arrays.asList("b2", "f2", "d1", "d3")));
        pointNeighbours.put("d3", new HashSet<>(Arrays.asList("c3", "e3", "d2")));
        pointNeighbours.put("d5", new HashSet<>(Arrays.asList("c5", "e5", "d6")));
        pointNeighbours.put("d6", new HashSet<>(Arrays.asList("b6", "f6", "d5", "d7")));
        pointNeighbours.put("d7", new HashSet<>(Arrays.asList("a7", "g7", "d6")));

        pointNeighbours.put("e3", new HashSet<>(Arrays.asList("e4", "d3")));
        pointNeighbours.put("e4", new HashSet<>(Arrays.asList("e3", "e5", "f4")));
        pointNeighbours.put("e5", new HashSet<>(Arrays.asList("e4", "d5")));

        pointNeighbours.put("f2", new HashSet<>(Arrays.asList("d2", "f4")));
        pointNeighbours.put("f4", new HashSet<>(Arrays.asList("e4", "g4", "f2", "f6")));
        pointNeighbours.put("f6", new HashSet<>(Arrays.asList("f4", "d6")));

        pointNeighbours.put("g1", new HashSet<>(Arrays.asList("g4", "d1")));
        pointNeighbours.put("g4", new HashSet<>(Arrays.asList("g1", "g7", "f4")));
        pointNeighbours.put("g7", new HashSet<>(Arrays.asList("g4", "d7")));
    }

    @Override
    protected void initMills() {
        mills.add(new HashSet<>(Arrays.asList("a1", "a4", "a7")));
        mills.add(new HashSet<>(Arrays.asList("b2", "b4", "b6")));
        mills.add(new HashSet<>(Arrays.asList("c3", "c4", "c5")));
        mills.add(new HashSet<>(Arrays.asList("d1", "d2", "d3")));
        mills.add(new HashSet<>(Arrays.asList("d5", "d6", "d7")));
        mills.add(new HashSet<>(Arrays.asList("e3", "e4", "e5")));
        mills.add(new HashSet<>(Arrays.asList("f2", "f4", "f6")));
        mills.add(new HashSet<>(Arrays.asList("g1", "g4", "g7")));
        mills.add(new HashSet<>(Arrays.asList("a1", "d1", "g1")));
        mills.add(new HashSet<>(Arrays.asList("b2", "d2", "f2")));
        mills.add(new HashSet<>(Arrays.asList("c3", "d3", "e3")));
        mills.add(new HashSet<>(Arrays.asList("a4", "b4", "c4")));
        mills.add(new HashSet<>(Arrays.asList("e4", "f4", "g4")));
        mills.add(new HashSet<>(Arrays.asList("c5", "d5", "e5")));
        mills.add(new HashSet<>(Arrays.asList("b6", "d6", "f6")));
        mills.add(new HashSet<>(Arrays.asList("a7", "d7", "g7")));
    }

    @Override
    public String toString() {

        return String.format("7  %s-----------%s-----------%s\n", getManSymbol("a7"), getManSymbol("d7"), getManSymbol("g7")) +
                "   |           |           |\n" +
                String.format("6  |   %s-------%s-------%s   |\n", getManSymbol("b6"), getManSymbol("d6"), getManSymbol("f6")) +
                "   |   |       |       |   |\n" +
                String.format("5  |   |   %s---%s---%s   |   |\n", getManSymbol("c5"), getManSymbol("d5"), getManSymbol("e5")) +
                "   |   |   |       |   |   |\n" +
                String.format("4  %s---%s---%s       %s---%s---%s\n", getManSymbol("a4"), getManSymbol("b4"), getManSymbol("c4"), getManSymbol("e4"), getManSymbol("f4"), getManSymbol("g4")) +
                "   |   |   |       |   |   |\n" +
                String.format("3  |   |   %s---%s---%s   |   |\n", getManSymbol("c3"), getManSymbol("d3"), getManSymbol("e3")) +
                "   |   |       |       |   |\n" +
                String.format("2  |   %s-------%s-------%s   |\n", getManSymbol("b2"), getManSymbol("d2"), getManSymbol("f2")) +
                "   |           |           |\n" +
                String.format("1  %s-----------%s-----------%s\n\n", getManSymbol("a1"), getManSymbol("d1"), getManSymbol("g1")) +
                "   A---B---C---D---E---F---G\n";
    }

    private String getManSymbol(String pointName) {
        switch (pointColor.get(pointName)) {
            case WHITE:
                return "W";
            case BLACK:
                return "B";
        }
        return " ";
    }

}
