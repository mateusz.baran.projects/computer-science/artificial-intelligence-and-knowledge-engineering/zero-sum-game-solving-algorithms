package com.gitlab.necky0.Mill.game.analysis;


import com.gitlab.necky0.Mill.game.IGame;
import com.gitlab.necky0.Mill.game.model.enumerator.Color;

public class MenDiffAvMoves extends GameAnalysis {

    public MenDiffAvMoves() {
    }


    protected double menDifferenceFactor(IGame game) {
        return MAN_FACTOR * (game.getModel().getPointsByColor().get(Color.WHITE).size() - game.getModel().getPointsByColor().get(Color.BLACK).size());
    }

    public double coefficient(IGame game) {
        return (menDifferenceFactor(game) + availableMovesFactor(game)) / menInGameFactor(game);
    }

}
