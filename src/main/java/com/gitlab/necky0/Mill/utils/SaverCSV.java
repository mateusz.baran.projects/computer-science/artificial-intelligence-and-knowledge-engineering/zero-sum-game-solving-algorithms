package com.gitlab.necky0.Mill.utils;


public class SaverCSV {

    private StringBuilder sb;

    public SaverCSV() {
        this.sb = new StringBuilder();
    }

    @Override
    public String toString() {
        return sb.toString();
    }

    public void add(String value) {
        sb.append(String.format("\"%s\", ", value));
    }

    public void addLastInRow(String value) {
        sb.append(String.format("\"%s\"\n", value));
    }
}
