package com.gitlab.necky0.Mill;

import com.gitlab.necky0.Mill.game.Game;
import com.gitlab.necky0.Mill.game.analysis.GameAnalysis;
import com.gitlab.necky0.Mill.game.analysis.MenDiffAvMovesFlying;
import com.gitlab.necky0.Mill.game.IGame;
import com.gitlab.necky0.Mill.game.algorithm.Algorithm;
import com.gitlab.necky0.Mill.game.algorithm.AlphaBetaPruning;
import com.gitlab.necky0.Mill.game.model.Classic;
import com.gitlab.necky0.Mill.game.model.Model;
import com.gitlab.necky0.Mill.game.model.enumerator.Color;
import com.gitlab.necky0.Mill.game.model.enumerator.State;

public class RunAIvsAI {
    public static void main(String[] args) {

        Model model = new Classic();
        IGame game = new Game(model);

        GameAnalysis gameStateAnalysis = new MenDiffAvMovesFlying();

        Algorithm white = new AlphaBetaPruning(game, gameStateAnalysis, 6, true, false);
        Algorithm black = new AlphaBetaPruning(game, gameStateAnalysis, 6, true, false);

//        Algorithm white = new MiniMax(game, gameStateAnalysis, 4);
//        Algorithm black = new MiniMax(game, gameStateAnalysis, 4);

        String command;

        do {
            System.out.println();
            System.out.println(game.getModel());
            System.out.println("Phase: " + game.getState());
            System.out.println("Player: " + game.getActivePlayerColor());
            System.out.println("Coefficient: " + gameStateAnalysis.coefficient(game));

            if (State.End.equals(game.getState())) {
                System.out.println();
                System.out.println("--- WINNER ---");
                System.out.println("--> " + game.getResult());
                System.out.println("--> " + game.getWinnerPlayerColor());
                break;
            }

            if (Color.WHITE.equals(game.getActivePlayerColor()))
                command = white.getNextMove();
            else
                command = black.getNextMove();

            System.out.println("Best Move: " + command);
            game.move(command);

        } while (true);

        System.out.println("Total moves: " + (game.getGameStateLastIndex() + 1));
    }
}
