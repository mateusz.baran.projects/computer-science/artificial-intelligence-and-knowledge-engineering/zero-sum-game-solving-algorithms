package com.gitlab.necky0.Mill.model;

import java.util.List;
import java.util.Map;

public class GameState {
    private Map<String, String> points;
    private String activePlayerColor;
    private String winnerPlayerColor;
    private List<String> possibleMoves;
    private List<String> movementRecords;
    private int placingMenLeft;
    private String state;
    private String result;
    private double gameStateValue;
    private boolean restoring;

    public GameState(Map<String, String> points, String activePlayerColor, String winnerPlayerColor, List<String> possibleMoves, List<String> movementRecords, int placingMenLeft, String state, String result, double gameStateValue, boolean restoring) {
        this.points = points;
        this.activePlayerColor = activePlayerColor;
        this.winnerPlayerColor = winnerPlayerColor;
        this.possibleMoves = possibleMoves;
        this.movementRecords = movementRecords;
        this.placingMenLeft = placingMenLeft;
        this.state = state;
        this.result = result;
        this.gameStateValue = gameStateValue;
        this.restoring = restoring;
    }

    public Map<String, String> getPoints() {
        return points;
    }

    public String getActivePlayerColor() {
        return activePlayerColor;
    }

    public String getWinnerPlayerColor() {
        return winnerPlayerColor;
    }

    public List<String> getPossibleMoves() {
        return possibleMoves;
    }

    public List<String> getMovementRecords() {
        return movementRecords;
    }

    public int getPlacingMenLeft() {
        return placingMenLeft;
    }

    public String getState() {
        return state;
    }

    public String getResult() {
        return result;
    }

    public double getGameStateValue() {
        return gameStateValue;
    }

    public boolean isRestoring() {
        return restoring;
    }
}
