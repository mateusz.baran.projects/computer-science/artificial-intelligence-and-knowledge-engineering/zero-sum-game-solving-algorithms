package com.gitlab.necky0.Mill.model;

public class AiMoveBody {
    private String heuristic;
    private String algorithm;

    public AiMoveBody(String heuristic, String algorithm) {
        this.heuristic = heuristic;
        this.algorithm = algorithm;
    }

    public String getHeuristic() {
        return heuristic;
    }

    public String getAlgorithm() {
        return algorithm;
    }

}
