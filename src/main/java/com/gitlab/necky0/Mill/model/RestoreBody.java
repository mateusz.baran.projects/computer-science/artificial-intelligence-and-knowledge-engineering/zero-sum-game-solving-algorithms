package com.gitlab.necky0.Mill.model;

public class RestoreBody {
    private int index;
    private String heuristic;

    public RestoreBody(int index, String heuristic) {
        this.index = index;
        this.heuristic = heuristic;
    }

    public int getIndex() {
        return index;
    }

    public String getHeuristic() {
        return heuristic;
    }

}
