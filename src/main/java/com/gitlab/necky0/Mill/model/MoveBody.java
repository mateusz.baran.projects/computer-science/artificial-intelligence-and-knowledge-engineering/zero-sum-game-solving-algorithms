package com.gitlab.necky0.Mill.model;

public class MoveBody {
    private String command;
    private String heuristic;

    public MoveBody(String command, String heuristic) {
        this.command = command;
        this.heuristic = heuristic;
    }

    public String getCommand() {
        return command;
    }

    public String getHeuristic() {
        return heuristic;
    }

}
