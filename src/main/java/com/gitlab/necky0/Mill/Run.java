package com.gitlab.necky0.Mill;


import com.gitlab.necky0.Mill.game.Game;
import com.gitlab.necky0.Mill.game.analysis.GameAnalysis;
import com.gitlab.necky0.Mill.game.analysis.MenDiffAvMovesFlying;
import com.gitlab.necky0.Mill.game.IGame;
import com.gitlab.necky0.Mill.game.algorithm.Algorithm;
import com.gitlab.necky0.Mill.game.algorithm.AlphaBetaPruning;
import com.gitlab.necky0.Mill.game.model.Classic;
import com.gitlab.necky0.Mill.game.model.Model;
import com.gitlab.necky0.Mill.game.model.enumerator.State;

public class Run {
    public static void main(String[] args) {

        Model model = new Classic();
        IGame game = new Game(model);

        GameAnalysis gameStateAnalysis = new MenDiffAvMovesFlying();

//        String[] moves = {
//                "a1", "b2", "a4", "b4", "a7", "b4", "b4", "d7", "b6", "d7", "c4", "d1", "e5", "g7", "g4", "e4",
//                "g1", "f6", "e3", "f2", "a7-d7", "b6-d6", "d7-a7", "f6"
//
//        };

        Algorithm algorithm = new AlphaBetaPruning(game, gameStateAnalysis, 1, true, false);

        String[] moves = {
                "d2", "f2", "d6", "f4", "f6"

        };

//        String[] moves = {
//                "a1", "b2", "a4", "b4", "a7", "b4", "b4", "d7", "d6", "g7", "d6", "b6", "a1", "d1", "d2", "a1"
//        };

//        String[] moves = {
//                "a1", "b2", "a4", "b4", "a7", "b4", "b4", "d7", "d6", "g7", "d6", "b6", "d6", "d2", "f2", "d1", "d3",
//                "f4", "e4", "g4", "e4-e5", "g4-g1"
//        };

        for (String move : moves) {
            System.out.println(game.getActivePlayerColor() + " - " + move);
            game.move(move);

            System.out.println();
            System.out.println(game.getModel());
            System.out.println(game.getState());
            System.out.println(game.getActivePlayerColor());
            System.out.println(game.getPossibleMoves());
            System.out.println("Coefficient: " + gameStateAnalysis.coefficient(game));
            System.out.println("Best Move: " + algorithm.getNextMove());

            if (State.End.equals(game.getState())) {
                System.out.println();
                System.out.println("--- WINNER ---");
                System.out.println(game.getResult());
                System.out.println(game.getWinnerPlayerColor());
            }
        }
    }
}
