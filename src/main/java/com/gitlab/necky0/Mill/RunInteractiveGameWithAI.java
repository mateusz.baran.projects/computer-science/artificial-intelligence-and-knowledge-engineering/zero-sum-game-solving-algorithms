package com.gitlab.necky0.Mill;

import com.gitlab.necky0.Mill.game.Game;
import com.gitlab.necky0.Mill.game.analysis.GameAnalysis;
import com.gitlab.necky0.Mill.game.analysis.MenDiffAvMovesFlying;
import com.gitlab.necky0.Mill.game.IGame;
import com.gitlab.necky0.Mill.game.algorithm.Algorithm;
import com.gitlab.necky0.Mill.game.algorithm.AlphaBetaPruning;
import com.gitlab.necky0.Mill.game.model.Classic;
import com.gitlab.necky0.Mill.game.model.Model;
import com.gitlab.necky0.Mill.game.model.enumerator.Color;
import com.gitlab.necky0.Mill.game.model.enumerator.State;

import java.util.Scanner;

public class RunInteractiveGameWithAI {
    public static void main(String[] args) {

        Model model = new Classic();
        IGame game = new Game(model);

        GameAnalysis gameStateAnalysis = new MenDiffAvMovesFlying();

//        Algorithm algorithm1 = new MiniMax(game, gameStateAnalysis, 4);
        Algorithm algorithm = new AlphaBetaPruning(game, gameStateAnalysis, 6, true, false);

        Scanner scanner = new Scanner(System.in);
        String command;
        do {
            System.out.println();
            System.out.println(game.getModel());
            System.out.println("Phase: " + game.getState());
            System.out.println("Player: " + game.getActivePlayerColor());
            System.out.println("Player Coefficient: " + gameStateAnalysis.activePlayerCoefficient(game));
            System.out.println("Moves: " + game.getMovementRecords());



            if (State.End.equals(game.getState())) {
                System.out.println();
                System.out.println("--- WINNER ---");
                System.out.println("--> " + game.getResult());
                System.out.println("--> " + game.getWinnerPlayerColor());
                break;
            }

            if (Color.BLACK.equals(game.getActivePlayerColor())) {
                System.out.println("Available moves: " + game.getPossibleMoves());
                System.out.print("\nEnter your move: ");
                command = scanner.nextLine();
                if ("back".equals(command) && game.getGameStateLastIndex() > 1) {
                    game = game.getGameState(game.getGameStateLastIndex() - 2);
                    algorithm.setGame(game);
                } else {
                    game.move(command);
                }
            } else {
                command = algorithm.getNextMove();
                System.out.println("Best Move: " + command);
                game.move(command);
            }

        } while (!"exit".equals(command));
    }
}
