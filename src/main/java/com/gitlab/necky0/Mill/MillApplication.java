package com.gitlab.necky0.Mill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MillApplication {

	public static void main(String[] args) {
		SpringApplication.run(MillApplication.class, args);
	}

}
